import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { ClinicsComponent } from './components/clinics/clinics.component';
import { OffersComponent } from './components/offers/offers.component';
import { ServicesComponent } from './components/services/services.component';
import { MedicsComponent } from './components/medics/medics.component';


const routes: Routes = [
  { path: '', component: DashboardComponent},
  { path: 'clinics', component: ClinicsComponent},
  { path: 'offers', component: OffersComponent},
  { path: 'services', component: ServicesComponent},
  { path: 'medics', component: MedicsComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
