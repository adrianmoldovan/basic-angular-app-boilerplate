import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-base-layout',
  templateUrl: './base-layout.component.html',
  styleUrls: ['./base-layout.component.css']
})
export class BaseLayoutComponent implements OnInit {

  @Input() appName: string;
  @Input() appInfo: any;

  constructor() { }

  ngOnInit(): void {
  }

}
