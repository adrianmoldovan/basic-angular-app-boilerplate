import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-clinics',
  templateUrl: './clinics.component.html',
  styleUrls: ['./clinics.component.css']
})
export class ClinicsComponent implements OnInit {

  interfaceName :string = "Clinici";

  items: any[] = [
    {
      name: 'Clinica 1'
    },
    {
      name: 'Clinica 2'
    },
    {
      name: 'Clinica 3'
    },
    {
      name: 'Clinica 4'
    },
    {
      name: 'Clinica 5'
    },
  ];

  constructor() { }

  ngOnInit(): void {
  }

}
