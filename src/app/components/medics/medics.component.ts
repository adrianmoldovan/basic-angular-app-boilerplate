import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-medics',
  templateUrl: './medics.component.html',
  styleUrls: ['./medics.component.css']
})
export class MedicsComponent implements OnInit {

  interfaceName :string = "Medici";

  items: any[] = [
    {
      name: 'Medicul 1'
    },
    {
      name: 'Medicul 2'
    },
    {
      name: 'Medicul 3'
    },
    {
      name: 'Medicul 4'
    },
    {
      name: 'Medicul 5'
    },
  ];

  constructor() { }

  ngOnInit(): void {
  }

}
