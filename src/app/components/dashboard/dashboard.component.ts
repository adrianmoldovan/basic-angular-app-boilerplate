import { Component, OnInit } from '@angular/core';
import { DashboardWidgetModel } from '../../models/dashboard-widget-model';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  interfaceName :string = "Panou general";

  widgets: DashboardWidgetModel[] = [
    {
      index: 0, 
      route: "clinics", 
      icon: "business",
      title: "50 Clinici medicale", 
      text: "Cauta clinici medicale"
    },
    {
      index: 1, 
      route: "offers",
      icon: "local_offer",
      title: "9 Oferte speciale", 
      text: "Cauta oferte speciale"
    },
    {
      index: 2, 
      route: "services", 
      icon: "medical_services",
      title: "136 Servicii medicale", 
      text: "Cauta servicii medicale"
    },
    {
      index: 3, 
      route: "medics", 
      icon: "assignment_ind",
      title: "62 Medici specialisti", 
      text: "Cauta medici specialisti"
    }
  ];

  constructor() { }

  ngOnInit(): void {
  }

}
