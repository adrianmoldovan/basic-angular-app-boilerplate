import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'DENTbooking';
  appInfo = {
    name: ['DENT','bookig'],
    version: '0.0.1'
  }
}
