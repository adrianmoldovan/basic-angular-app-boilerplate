import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-interface-header',
  templateUrl: './interface-header.component.html',
  styleUrls: ['./interface-header.component.css']
})
export class InterfaceHeaderComponent implements OnInit {

  @Input() interfaceName;
  @Input() userName;

  constructor() { }

  ngOnInit(): void {
  }

}
