import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-offers',
  templateUrl: './offers.component.html',
  styleUrls: ['./offers.component.css']
})
export class OffersComponent implements OnInit {

  interfaceName :string = "Oferte";

  items: any[] = [
    {
      name: 'Oferta 1'
    },
    {
      name: 'Oferta 2'
    },
    {
      name: 'Oferta 3'
    },
    {
      name: 'Oferta 4'
    },
    {
      name: 'Oferta 5'
    },
  ];

  constructor() { }

  ngOnInit(): void {
  }

}
