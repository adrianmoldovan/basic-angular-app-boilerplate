import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-services',
  templateUrl: './services.component.html',
  styleUrls: ['./services.component.css']
})
export class ServicesComponent implements OnInit {

  interfaceName :string = "Servicii";

  items: any[] = [
    {
      name: 'Serviciul 1'
    },
    {
      name: 'Serviciul 2'
    },
    {
      name: 'Serviciul 3'
    },
    {
      name: 'Serviciul 4'
    },
    {
      name: 'Serviciul 5'
    },
  ];

  constructor() { }

  ngOnInit(): void {
  }

}
