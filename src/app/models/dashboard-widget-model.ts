export class DashboardWidgetModel{
    index: number; 
    route: string 
    icon: string;
    title: string; 
    text: string;
}