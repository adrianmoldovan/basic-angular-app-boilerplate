import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { FlexLayoutModule } from '@angular/flex-layout';

import { MatToolbarModule } from '@angular/material/toolbar';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { MatCardModule } from '@angular/material/card';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { BaseLayoutComponent } from './layout/base-layout/base-layout.component';
import { SimpleWidgetComponent } from './layout/simple-widget/simple-widget.component';
import { DashboardWidgetComponent } from './layout/dashboard-widget/dashboard-widget.component';

import { DashboardComponent } from './components/dashboard/dashboard.component';
import { ClinicsComponent } from './components/clinics/clinics.component';
import { OffersComponent } from './components/offers/offers.component';
import { ServicesComponent } from './components/services/services.component';
import { InterfaceHeaderComponent } from './layout/interface-header/interface-header.component';
import { MedicsComponent } from './components/medics/medics.component';

@NgModule({
  declarations: [
    AppComponent,
    BaseLayoutComponent,
    DashboardComponent,
    ClinicsComponent,
    OffersComponent,
    ServicesComponent,
    SimpleWidgetComponent,
    DashboardWidgetComponent,
    InterfaceHeaderComponent,
    MedicsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FlexLayoutModule, 
    MatToolbarModule,
    MatSidenavModule,
    MatListModule,  
    MatButtonModule,
    MatIconModule,
    MatCardModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
